<?php

declare(strict_types=1);

namespace Achievements\Domain\Service;

use Achievements\Domain\DataTransferObject\AchievementDTOInterface;
use Achievements\Domain\DataTransferObject\BadgeDTOInterface;
use Achievements\Domain\Model\BadgeInterface;

/**
 * Interface AchievementSaver.
 */
interface AchievementCreatorInterface
{
    /**
     * @param AchievementDTOInterface $achievementDTO
     *
     * @return AchievementDTOInterface
     */
    public function createAchievement(AchievementDTOInterface $achievementDTO): AchievementDTOInterface;

    /**
     * @param BadgeDTOInterface $badgeDTO
     *
     * @return BadgeInterface
     */
    public function createBadge(BadgeDTOInterface $badgeDTO): BadgeInterface;

    /**
     * @param AchievementDTOInterface $achievement
     * @param BadgeDTOInterface $badge
     *
     * @return BadgeInterface
     */
    public function attachAchievementToBadge(
        AchievementDTOInterface $achievement,
        BadgeDTOInterface $badge
    ): BadgeInterface;
}
