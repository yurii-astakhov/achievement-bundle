<?php

declare(strict_types=1);

namespace Achievements\Domain\Dictionary;

/**
 * Class AchievementType.
 */
final class AchievementStrategyType
{
    public const ACTION = 'action';
    public const STATE = 'state';
    public const PROGRESS = 'progress';
}
