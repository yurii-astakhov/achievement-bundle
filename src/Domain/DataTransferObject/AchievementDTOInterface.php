<?php

declare(strict_types=1);

namespace Achievements\Domain\DataTransferObject;

/**
 * Interface AchievementDTO.
 */
interface AchievementDTOInterface
{
    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return int|null
     */
    public function getStepsToAchieve(): ?int;
}
