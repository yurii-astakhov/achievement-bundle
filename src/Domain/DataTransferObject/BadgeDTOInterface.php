<?php

declare(strict_types=1);

namespace Achievements\Domain\DataTransferObject;

/**
 * Interface BadgeDTO.
 */
interface BadgeDTOInterface
{
    public function getName(): string;

    public function getDescription(): string;
}
