<?php

declare(strict_types=1);

namespace Achievements\Domain\Model;

use DateTimeInterface;

/**
 * Interface AchievementGrandInterface.
 */
interface AchievementReceivedInterface
{
    /**
     * @return string
     */
    public function getUserId(): string;

    /**
     * @param string $userId
     *
     * @return AchievementReceivedInterface
     */
    public function setUserId(string $userId): AchievementReceivedInterface;

    /**
     * @return AchievementInterface
     */
    public function getAchievement(): AchievementInterface;

    /**
     * @param AchievementInterface $achievement
     *
     * @return AchievementReceivedInterface
     */
    public function setAchievement(AchievementInterface $achievement): AchievementReceivedInterface;

    /**
     * @return float
     */
    public function getProgress(): float;

    /**
     * @param float $progress
     *
     * @return AchievementReceivedInterface
     */
    public function setProgress(float $progress): AchievementReceivedInterface;

    /**
     * @return DateTimeInterface
     */
    public function getReceivedAt(): DateTimeInterface;

    /**
     * @param DateTimeInterface $receivedAt
     *
     * @return AchievementReceivedInterface
     */
    public function setReceivedAt(DateTimeInterface $receivedAt): AchievementReceivedInterface;

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface;
}
