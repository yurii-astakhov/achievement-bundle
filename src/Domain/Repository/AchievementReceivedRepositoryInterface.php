<?php

namespace Achievements\Domain\Repository;

use Achievements\Domain\Model\AchievementInterface;
use Achievements\Domain\Model\AchievementReceivedInterface;

/**
 * Interface AchievementReceivedRepositoryInterface.
 */
interface AchievementReceivedRepositoryInterface
{
    /**
     * @param AchievementReceivedInterface $achievementGrand
     *
     * @return AchievementReceivedInterface
     */
    public function save(AchievementReceivedInterface $achievementGrand): AchievementReceivedInterface;

    /**
     * @param AchievementInterface $achievement
     * @param string $userId
     *
     * @return AchievementReceivedInterface|null
     */
    public function findByAchievementAndUserId(AchievementInterface $achievement, string $userId): ?AchievementReceivedInterface;

    /**
     * @param string $userId
     *
     * @return AchievementReceivedInterface[]
     */
    public function findByUserId(string $userId): array;
}