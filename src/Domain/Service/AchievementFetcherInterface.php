<?php

declare(strict_types=1);

namespace Achievements\Domain\Service;

use Achievements\Domain\DataTransferObject\AchievementDTOInterface;
use Achievements\Domain\DataTransferObject\BadgeDTOInterface;

/**
 * Interface AchievementFetcherInterface.
 */
interface AchievementFetcherInterface
{
    /**
     * @param string $name
     *
     * @return AchievementDTOInterface
     */
    public function fetchAchievement(string $name): AchievementDTOInterface;

    /**
     * @param string $name
     *
     * @return BadgeDTOInterface
     */
    public function fetchBadge(string $name): BadgeDTOInterface;

    /**
     * @return AchievementDTOInterface[]
     */
    public function fetchAchievements(): array;
}
