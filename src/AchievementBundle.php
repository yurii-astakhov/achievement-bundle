<?php

declare(strict_types=1);

namespace Achievements;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class AchievementBundle.
 */
class AchievementBundle extends Bundle
{

}
