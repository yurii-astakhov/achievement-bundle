<?php

declare(strict_types=1);

namespace Achievements\Domain\DataTransferObject;

use DateTimeInterface;

/**
 * Interface ReceivedAchievementDTOInterface.
 */
interface ReceivedAchievementDTOInterface extends AchievementDTOInterface
{
    /**
     * @return float
     */
    public function getProgress(): float;

    /**
     * @return DateTimeInterface|null
     */
    public function getReceivedAt(): ?DateTimeInterface;

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface;
}
