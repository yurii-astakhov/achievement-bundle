<?php

declare(strict_types=1);

namespace Achievements\Domain\Model;

/**
 * Interface BadgeInterface.
 */
interface BadgeInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @return AchievementInterface[]
     */
    public function getAchievements(): array;

    /**
     * @param string $name
     *
     * @return BadgeInterface
     */
    public function setName(string $name): BadgeInterface;

    /**
     * @param string $description
     *
     * @return BadgeInterface
     */
    public function setDescription(string $description): BadgeInterface;

    /**
     * @param AchievementInterface $achievement
     *
     * @return BadgeInterface
     */
    public function addAchievement(AchievementInterface $achievement): BadgeInterface;
}
