<?php

declare(strict_types=1);

namespace Achievements\Domain\Model;

/**
 * Interface AchievementInterface.
 */
interface AchievementInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return string|null
     */
    public function getGrandType(): ?string;

    /**
     * @return string|null
     */
    public function getIcon(): ?string;

    /**
     * @param string $name
     *
     * @return AchievementInterface
     */
    public function setName(string $name): AchievementInterface;

    /**
     * @param string $description
     *
     * @return AchievementInterface
     */
    public function setDescription(string $description): AchievementInterface;
}
