<?php

namespace Achievements\Domain\Repository;

use Achievements\Domain\Model\AchievementInterface;

interface AchievementRepositoryInterface
{
    /**
     * @param AchievementInterface $achievement
     *
     * @return AchievementInterface
     */
    public function save(AchievementInterface $achievement): AchievementInterface;

    /**
     * @param string $name
     *
     * @return AchievementInterface|null
     */
    public function findByName(string $name): ?AchievementInterface;

    /**
     * @return AchievementInterface[]
     */
    public function all(): array;
}